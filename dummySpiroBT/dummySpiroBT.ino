const int16_t vrealtime[]={0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	2,	5,	10,	14,	19,	24,	29,	34,	39,	44,	50,	56,	62,	68,	74,	80,	84,	88,	89,	90,	89,	86,	82,	78,	73,	69,	64,	60,	56,	52,	48,	44,	40,	36,	32,	29,	25,	22,	19,	16,	13,	11,	9,	8,	6,	5,	4,	3,	2,	1,	1,	1,	1,	1,	1,	1,	2,	5,	9,	13,	17,	21,	25,	29,	33,	36,	40,	44,	47,	51,	55,	58,	62,	66,	69,	73,	76,	80,	82,	84,	85,	86,	85,	84,	83,	82,	79,	75,	72,	68,	65,	62,	58,	55,	51,	48,	45,	42,	39,	36,	33,	29,	26,	23,	20,	17,	15,	13,	11,	9,	7,	6,	5,	4,	3,	3,	2,	2,	3,	3,	3,	3,	3,	3,	4,	5,	6,	9,	12,	16,	20,	24,	29,	33,	37,	42,	47,	51,	55,	60,	65,	71,	76,	81,	87,	91,	96,	100,	104,	107,	111,	115,	118,	122,	125,	128,	131,	134,	137,	140,	143,	146,	148,	150,	152,	154,	156,	158,	160,	162,	164,	167,	168,	170,	171,	173,	173,	174,	174,	173,	173,	174,	174,	175,	176,	162,	139,	115,	92,	72,	54,	40,	27,	15,	4,	-5,	-14,	-21,	-29,	-35,	-41,	-46,	-51,	-55,	-59,	-62,	-65,	-68,	-71,	-73,	-75,	-77,	-78,	-79,	-80,	-81,	-82,	-82,	-83,	-83,	-84,	-84,	-85,	-85,	-86,	-86,	-86,	-87,	-88,	-88,	-88,	-88,	-88,	-88,	-88,	-87,	-87,	-87,	-86,	-86,	-86,	-86,	-86,	-85,	-82,	-75,	-67,	-60,	-52,	-43,	-35,	-26,	-17,	-8,	1,	9,	18,	27,	36,	45,	54,	63,	72,	80,	88,	96,	103,	109,	115,	120,	124,	129,	133,	137,	141,	145,	148,	151,	152,	154,	156,	157,	157,	157,	156,	156,	157,	158,	160,	162,	162,	161,	162,	163,	164,	165,	164,	162,	162,	162,	161,	158,	153,	148,	142,	136,	130,	124,	119,	113,	107,	101,	95,	90,	85,	80,	75,	71,	67,	63,	60,	56,	51,	47,	43,	40,	36,	33,	30,	27,	24,	20,	17,	15,	13,	11,	9,	7,	6,	5,	4,	3,	3,	2,	2,	2,	1,	1 };
const float vtimeseries[] = {0,	0.14,	0.37,	0.62,	0.85,	1.05,	1.22,	1.37,	1.5,	1.61,	1.72,	1.81,	1.9,	1.98,	2.05,	2.12,	2.17,	2.23,	2.27,	2.31,	2.35,	2.39,	2.42,	2.44,	2.47,	2.49,	2.51,	2.53,	2.55,	2.55,	2.56,	2.58,	2.58,	2.59,	2.59,	2.6,	2.6,	2.61,	2.61,	2.62,	2.63,	2.63,	2.63,	2.63,	2.64,	2.64,	2.64,	2.64,	2.64,	2.64,	2.64,	2.64,	2.64,	2.63,	2.63,	2.63,	2.63,	2.63,	2.63 };
const int16_t fgraph[] = {-12,	140,	233,	246,	229,	200,	174,	144,	130,	119,	109,	90,	87,	74,	75,	64,	57,	52,	47,	41,	38,	34,	30,	29,	27,	23,	19,	17,	13,	9,	9,	13,	4,	7,	4,	4,	7,	6,	2,	6,	7,	2,	1,	4,	8,	1,	3,	1,	0,	-1,	-3,	-1,	-2,	-3,	-6,	-3,	2,	0,	0,	-9,	-37,	-66,	-77,	-77,	-80,	-86,	-85,	-87,	-86,	-90,	-89,	-84,	-87,	-89,	-91,	-90,	-89,	-87,	-90,	-86,	-80,	-75,	-69,	-61,	-57,	-51,	-44,	-45,	-41,	-40,	-41,	-38,	-32,	-27,	-18,	-18,	-16,	-11,	-6,	4,	10,	-4,	-10,	-8,	-18,	-17,	2,	2,	-8,	-8,	-9,	-10,	9,	17,	3 };
const int16_t vgraph[] = {0,	14,	37,	62,	85,	105,	122,	137,	150,	161,	172,	181,	190,	198,	205,	212,	217,	223,	227,	231,	235,	239,	242,	244,	247,	249,	251,	253,	255,	255,	256,	258,	258,	259,	259,	260,	260,	261,	261,	262,	263,	263,	263,	263,	264,	264,	264,	264,	264,	264,	264,	264,	264,	263,	263,	263,	263,	263,	263,	262,	258,	251,	244,	236,	228,	219,	211,	202,	194,	185,	176,	167,	159,	150,	140,	131,	123,	114,	105,	96,	88,	81,	74,	68,	62,	57,	52,	48,	44,	40,	36,	32,	28,	26,	24,	22,	21,	20,	19,	19,	20,	20,	19,	18,	16,	15,	15,	15,	14,	13,	13,	12,	12,	14,	14 };
const float vfgraph[][2] = { {0,	-0.12} ,
{0.14,	1.4} ,
{0.37,	2.33} ,
{0.62,	2.46} ,
{0.85,	2.29} ,
{1.05,	2} ,
{1.22,	1.74} ,
{1.37,	1.44} ,
{1.5,	1.3} ,
{1.61,	1.19} ,
{1.72,	1.09} ,
{1.81,	0.9} ,
{1.9,	0.87} ,
{1.98,	0.74} ,
{2.05,	0.75} ,
{2.12,	0.64} ,
{2.17,	0.57} ,
{2.23,	0.52} ,
{2.27,	0.47} ,
{2.31,	0.41} ,
{2.35,	0.38} ,
{2.39,	0.34} ,
{2.42,	0.3} ,
{2.44,	0.29} ,
{2.47,	0.27} ,
{2.49,	0.23} ,
{2.51,	0.19} ,
{2.53,	0.17} ,
{2.55,	0.13} ,
{2.55,	0.09} ,
{2.56,	0.09} ,
{2.58,	0.13} ,
{2.58,	0.04} ,
{2.59,	0.07} ,
{2.59,	0.04} ,
{2.6,	0.04} ,
{2.6,	0.07} ,
{2.61,	0.06} ,
{2.61,	0.02} ,
{2.62,	0.06} ,
{2.63,	0.07} ,
{2.63,	0.02} ,
{2.63,	0.01} ,
{2.63,	0.04} ,
{2.64,	0.08} ,
{2.64,	0.01} ,
{2.64,	0.03} ,
{2.64,	0.01} ,
{2.64,	0} ,
{2.64,	-0.01} ,
{2.64,	-0.03} ,
{2.64,	-0.01} ,
{2.64,	-0.02} ,
{2.63,	-0.03} ,
{2.63,	-0.06} ,
{2.63,	-0.03} ,
{2.63,	0.02} ,
{2.63,	0} ,
{2.63,	0} ,
{2.62,	-0.09} ,
{2.58,	-0.37} ,
{2.51,	-0.66} ,
{2.44,	-0.77} ,
{2.36,	-0.77} ,
{2.28,	-0.8} ,
{2.19,	-0.86} ,
{2.11,	-0.85} ,
{2.02,	-0.87} ,
{1.94,	-0.86} ,
{1.85,	-0.9} ,
{1.76,	-0.89} ,
{1.67,	-0.84} ,
{1.59,	-0.87} ,
{1.5,	-0.89} ,
{1.4,	-0.91} ,
{1.31,	-0.9} ,
{1.23,	-0.89} ,
{1.14,	-0.87} ,
{1.05,	-0.9} ,
{0.96,	-0.86} ,
{0.88,	-0.8} ,
{0.81,	-0.75} ,
{0.74,	-0.69} ,
{0.68,	-0.61} ,
{0.62,	-0.57} ,
{0.57,	-0.51} ,
{0.52,	-0.44} ,
{0.48,	-0.45} ,
{0.44,	-0.41} ,
{0.4,	-0.4} ,
{0.36,	-0.41} ,
{0.32,	-0.38} ,
{0.28,	-0.32} ,
{0.26,	-0.27} ,
{0.24,	-0.18} ,
{0.22,	-0.18} ,
{0.21,	-0.16} ,
{0.2,	-0.11} ,
{0.19,	-0.06} ,
{0.19,	0.04} ,
{0.2,	0.1} ,
{0.2,	-0.04} ,
{0.19,	-0.1} ,
{0.18,	-0.08} ,
{0.16,	-0.18} ,
{0.15,	-0.17} ,
{0.15,	0.02} ,
{0.15,	0.02} ,
{0.14,	-0.08} ,
{0.13,	-0.08} ,
{0.13,	-0.09} ,
{0.12,	-0.1} ,
{0.12,	0.09} ,
{0.14,	0.17} ,
{0.14,	0.03} 
};
#include "BluetoothSerial.h"
#include <ArduinoJson.h>
#include "src/TaskScheduler/TaskScheduler.h"
int stream_idx;
char cmd;
BluetoothSerial SerialBT;
StaticJsonDocument<256> graph;
StaticJsonDocument<2048> timeseries;
StaticJsonDocument<256> result;
StaticJsonDocument<150> in;
char str_temp[7];

bool result_send = false ;
char rxValue;
String msg;
Scheduler userScheduler;

Task streamVolume (100, TASK_FOREVER, []() {
  if(stream_idx<360){
    dtostrf(vrealtime[stream_idx]*0.01, 5, 2, str_temp);
    SerialBT.println(str_temp);
    Serial.println(str_temp);
	stream_idx++;
  } else {
    SerialBT.println('0');
  }
});
Task BTcallback (100, TASK_FOREVER, []() {
  while (SerialBT.available()) {
    rxValue = SerialBT.read();
	msg+=rxValue;
	if(rxValue=='}'){
	  DeserializationError error = deserializeJson(in, msg);
	  if (error) {
	    Serial.print(F("deserializeJson() failed: "));
	    Serial.println(error.f_str());
	    return;
	  } else {
	    msg="";
	    const char *cmd = in["cmd"];
        const char *mode = in["mode"];
		if( !strcmp(mode,"spiro") ){
		  if( !strcmp(cmd,"start") ){
		    stream_idx=0;
			streamVolume.enable();
		  } else if( !strcmp(cmd,"stop") ){
		    streamVolume.disable();
			if(stream_idx >=250){
			  // step_l=1;
			  // i=0;
			  result_send = true;
			}
		  }
		} 
	  }
	}
  }
});

void setup(){
  Serial.begin(115200);
  SerialBT.begin("respinos01"); //Bluetooth device name
  
  Serial.println("The device started, now you can pair it with bluetooth!");
  
  userScheduler.addTask(streamVolume);
  userScheduler.addTask(BTcallback);
  BTcallback.enable();
  userScheduler.startNow();
  graph.clear();
  for(int i=0; i<115; i++){
    copyArray(vfgraph[i], graph.to<JsonArray>());
    serializeJson(graph, SerialBT);
    serializeJson(graph, Serial);
  }
}
void loop(){

  userScheduler.execute();
  if(result_send){
    if(stream_idx >=250){
	  //send result
	
	  SerialBT.print("{\"value\": ");
	  Serial.print("{\"value\": ");
	  result.clear();
      result["FVC"] = 2.64;
      result["FEV1"] = 1.72;
      result["PEF"] = 2.46;
      result["ratio"] = 0.65;
      result["TimeSeriesLength"] = 59;
      result["XYLength"] = 115;
	  // String msg;
      serializeJson(result, SerialBT);
      serializeJson(result, Serial);
	  // SerialBT.write(msg.c_str());
	  
	  timeseries.clear();
	
	  SerialBT.print(", \"graph\": ");
	  Serial.print(", \"graph\": ");
      copyArray(vtimeseries, timeseries.to<JsonArray>());
	  SerialBT.print("{\"timeseries\": ");
	  Serial.print("{\"timeseries\": ");
      serializeJson(timeseries, SerialBT);
      serializeJson(timeseries, Serial);
	  SerialBT.print(",\"XY\": [ ");
	  Serial.print(",\"XY\": [ ");
	  graph.clear();
	  for(int i=0; i<115; i++){
        copyArray(vfgraph[i], graph.to<JsonArray>());
        serializeJson(graph, SerialBT);
        serializeJson(graph, Serial);
	    if(i<114){
	      SerialBT.write(',');
	      Serial.print(",");
	    }
	  }
	  SerialBT.print("] } }");
	  Serial.print("] } }");
	}
    result_send=false;
  }

/*   if (SerialBT.available()) {
    cmd = SerialBT.read();
	Serial.println(cmd);
	if(cmd == 's'){
	  streamVolume.disable();
	  if(stream_idx >=250){
	    //send result
		
	    SerialBT.print("{\"value\": ");
	    Serial.print("{\"value\": ");
	    result.clear();
        result["FVC"] = 2.64;
        result["FEV1"] = 1.72;
        result["PEF"] = 2.46;
        result["rasio"] = 0.65;
        result["VTLength"] = 59;
        result["FVLength"] = 115;
	    // String msg;
        serializeJson(result, SerialBT);
        serializeJson(result, Serial);
	    // SerialBT.write(msg.c_str());
	    
	    timeseries.clear();
		
	    SerialBT.print(", \"graph\": ");
	    Serial.print(", \"graph\": ");
        copyArray(vtimeseries, timeseries.to<JsonArray>());
	    SerialBT.print("{\"timeseries\": ");
	    Serial.print("{\"timeseries\": ");
        serializeJson(timeseries, SerialBT);
        serializeJson(timeseries, Serial);
	    SerialBT.print(",\"XY\": [ ");
	    Serial.print(",\"XY\": [ ");
	    graph.clear();
	    for(int i=0; i<115; i++){
          copyArray(vfgraph[i], graph.to<JsonArray>());
          serializeJson(graph, SerialBT);
          serializeJson(graph, Serial);
		  if(i<114){
		    SerialBT.write(',');
	        Serial.print(",");
		  }
	    }
	    SerialBT.print("] } }");
	    Serial.print("] } }");
	  }
	}
	else if (cmd == 'r'){
	  stream_idx=0;
	  streamVolume.enable();
	}
  } */
}


