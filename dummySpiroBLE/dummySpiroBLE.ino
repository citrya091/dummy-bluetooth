const int16_t vrealtime[]={0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	2,	5,	10,	14,	19,	24,	29,	34,	39,	44,	50,	56,	62,	68,	74,	80,	84,	88,	89,	90,	89,	86,	82,	78,	73,	69,	64,	60,	56,	52,	48,	44,	40,	36,	32,	29,	25,	22,	19,	16,	13,	11,	9,	8,	6,	5,	4,	3,	2,	1,	1,	1,	1,	1,	1,	1,	2,	5,	9,	13,	17,	21,	25,	29,	33,	36,	40,	44,	47,	51,	55,	58,	62,	66,	69,	73,	76,	80,	82,	84,	85,	86,	85,	84,	83,	82,	79,	75,	72,	68,	65,	62,	58,	55,	51,	48,	45,	42,	39,	36,	33,	29,	26,	23,	20,	17,	15,	13,	11,	9,	7,	6,	5,	4,	3,	3,	2,	2,	3,	3,	3,	3,	3,	3,	4,	5,	6,	9,	12,	16,	20,	24,	29,	33,	37,	42,	47,	51,	55,	60,	65,	71,	76,	81,	87,	91,	96,	100,	104,	107,	111,	115,	118,	122,	125,	128,	131,	134,	137,	140,	143,	146,	148,	150,	152,	154,	156,	158,	160,	162,	164,	167,	168,	170,	171,	173,	173,	174,	174,	173,	173,	174,	174,	175,	176,	162,	139,	115,	92,	72,	54,	40,	27,	15,	4,	-5,	-14,	-21,	-29,	-35,	-41,	-46,	-51,	-55,	-59,	-62,	-65,	-68,	-71,	-73,	-75,	-77,	-78,	-79,	-80,	-81,	-82,	-82,	-83,	-83,	-84,	-84,	-85,	-85,	-86,	-86,	-86,	-87,	-88,	-88,	-88,	-88,	-88,	-88,	-88,	-87,	-87,	-87,	-86,	-86,	-86,	-86,	-86,	-85,	-82,	-75,	-67,	-60,	-52,	-43,	-35,	-26,	-17,	-8,	1,	9,	18,	27,	36,	45,	54,	63,	72,	80,	88,	96,	103,	109,	115,	120,	124,	129,	133,	137,	141,	145,	148,	151,	152,	154,	156,	157,	157,	157,	156,	156,	157,	158,	160,	162,	162,	161,	162,	163,	164,	165,	164,	162,	162,	162,	161,	158,	153,	148,	142,	136,	130,	124,	119,	113,	107,	101,	95,	90,	85,	80,	75,	71,	67,	63,	60,	56,	51,	47,	43,	40,	36,	33,	30,	27,	24,	20,	17,	15,	13,	11,	9,	7,	6,	5,	4,	3,	3,	2,	2,	2,	1,	1 };
const float vtimeseries[] = {0,	0.14,	0.37,	0.62,	0.85,	1.05,	1.22,	1.37,	1.5,	1.61,	1.72,	1.81,	1.9,	1.98,	2.05,	2.12,	2.17,	2.23,	2.27,	2.31,	2.35,	2.39,	2.42,	2.44,	2.47,	2.49,	2.51,	2.53,	2.55,	2.55,	2.56,	2.58,	2.58,	2.59,	2.59,	2.6,	2.6,	2.61,	2.61,	2.62,	2.63,	2.63,	2.63,	2.63,	2.64,	2.64,	2.64,	2.64,	2.64,	2.64,	2.64,	2.64,	2.64,	2.63,	2.63,	2.63,	2.63,	2.63,	2.63 };
const float vfgraph[][2] = { {0,	-0.12} ,
{0.14,	1.4} ,
{0.37,	2.33} ,
{0.62,	2.46} ,
{0.85,	2.29} ,
{1.05,	2} ,
{1.22,	1.74} ,
{1.37,	1.44} ,
{1.5,	1.3} ,
{1.61,	1.19} ,
{1.72,	1.09} ,
{1.81,	0.9} ,
{1.9,	0.87} ,
{1.98,	0.74} ,
{2.05,	0.75} ,
{2.12,	0.64} ,
{2.17,	0.57} ,
{2.23,	0.52} ,
{2.27,	0.47} ,
{2.31,	0.41} ,
{2.35,	0.38} ,
{2.39,	0.34} ,
{2.42,	0.3} ,
{2.44,	0.29} ,
{2.47,	0.27} ,
{2.49,	0.23} ,
{2.51,	0.19} ,
{2.53,	0.17} ,
{2.55,	0.13} ,
{2.55,	0.09} ,
{2.56,	0.09} ,
{2.58,	0.13} ,
{2.58,	0.04} ,
{2.59,	0.07} ,
{2.59,	0.04} ,
{2.6,	0.04} ,
{2.6,	0.07} ,
{2.61,	0.06} ,
{2.61,	0.02} ,
{2.62,	0.06} ,
{2.63,	0.07} ,
{2.63,	0.02} ,
{2.63,	0.01} ,
{2.63,	0.04} ,
{2.64,	0.08} ,
{2.64,	0.01} ,
{2.64,	0.03} ,
{2.64,	0.01} ,
{2.64,	0} ,
{2.64,	-0.01} ,
{2.64,	-0.03} ,
{2.64,	-0.01} ,
{2.64,	-0.02} ,
{2.63,	-0.03} ,
{2.63,	-0.06} ,
{2.63,	-0.03} ,
{2.63,	0.02} ,
{2.63,	0} ,
{2.63,	0} ,
{2.62,	-0.09} ,
{2.58,	-0.37} ,
{2.51,	-0.66} ,
{2.44,	-0.77} ,
{2.36,	-0.77} ,
{2.28,	-0.8} ,
{2.19,	-0.86} ,
{2.11,	-0.85} ,
{2.02,	-0.87} ,
{1.94,	-0.86} ,
{1.85,	-0.9} ,
{1.76,	-0.89} ,
{1.67,	-0.84} ,
{1.59,	-0.87} ,
{1.5,	-0.89} ,
{1.4,	-0.91} ,
{1.31,	-0.9} ,
{1.23,	-0.89} ,
{1.14,	-0.87} ,
{1.05,	-0.9} ,
{0.96,	-0.86} ,
{0.88,	-0.8} ,
{0.81,	-0.75} ,
{0.74,	-0.69} ,
{0.68,	-0.61} ,
{0.62,	-0.57} ,
{0.57,	-0.51} ,
{0.52,	-0.44} ,
{0.48,	-0.45} ,
{0.44,	-0.41} ,
{0.4,	-0.4} ,
{0.36,	-0.41} ,
{0.32,	-0.38} ,
{0.28,	-0.32} ,
{0.26,	-0.27} ,
{0.24,	-0.18} ,
{0.22,	-0.18} ,
{0.21,	-0.16} ,
{0.2,	-0.11} ,
{0.19,	-0.06} ,
{0.19,	0.04} ,
{0.2,	0.1} ,
{0.2,	-0.04} ,
{0.19,	-0.1} ,
{0.18,	-0.08} ,
{0.16,	-0.18} ,
{0.15,	-0.17} ,
{0.15,	0.02} ,
{0.15,	0.02} ,
{0.14,	-0.08} ,
{0.13,	-0.08} ,
{0.13,	-0.09} ,
{0.12,	-0.1} ,
{0.12,	0.09} ,
{0.14,	0.17} ,
{0.14,	0.03} 
};
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

BLEServer *pServer = NULL;
BLECharacteristic * pTxCharacteristic;
bool deviceConnected = false;
bool oldDeviceConnected = false;
uint8_t txValue = 0;
int stream_idx;
String msg ;
String result ;
bool result_send = false ;
uint8_t step_l;
int i;

#include <ArduinoJson.h>
#include "src/TaskScheduler/TaskScheduler.h"
StaticJsonDocument<256> graph;
StaticJsonDocument<2048> timeseries;
StaticJsonDocument<256> value;
StaticJsonDocument<150> in;
char str_temp[9];

Scheduler userScheduler;

Task streamVolume (100, TASK_FOREVER, []() {
  if (deviceConnected) {
    if(stream_idx<360){
      dtostrf(vrealtime[stream_idx]*0.01, 5, 2, str_temp);
	  str_temp[7]='\r';
	  str_temp[8]='\n';
      pTxCharacteristic->setValue(str_temp);
      pTxCharacteristic->notify();
      Serial.println(str_temp);
	  stream_idx++;
    } else {
      dtostrf(0, 5, 2, str_temp);
      pTxCharacteristic->setValue(str_temp);
      pTxCharacteristic->notify();
    }
  }
});

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" // UART service UUID
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"


class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};

class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string rxValue = pCharacteristic->getValue();
	  
    
      if (rxValue.length() > 0) {
		msg="";
        for (int i = 0; i < rxValue.length(); i++){
          // Serial.print(rxValue[i]);
		  msg+=rxValue[i];
		}
        Serial.println(msg);
		DeserializationError error = deserializeJson(in, msg);
        if (error) {
		  Serial.print(F("deserializeJson() failed: "));
		  Serial.println(error.f_str());
		  return;
		}
		const char *cmd = in["cmd"];
        const char *mode = in["mode"];
		if( !strcmp(mode,"spiro") ){
		  if( !strcmp(cmd,"start") ){
		    stream_idx=0;
			streamVolume.enable();
		  } else if( !strcmp(cmd,"stop") ){
		    streamVolume.disable();
			if(stream_idx >=250){
			  step_l=1;
			  i=0;
			  result_send = true;
			}
		  }
		} 
      }
    }
};


void setup() {
  Serial.begin(115200);

  userScheduler.addTask(streamVolume);
  userScheduler.startNow();
  
  // Create the BLE Device
  BLEDevice::init("UART Service");

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pTxCharacteristic = pService->createCharacteristic(
										CHARACTERISTIC_UUID_TX,
										BLECharacteristic::PROPERTY_NOTIFY
									);
                      
  pTxCharacteristic->addDescriptor(new BLE2902());

  BLECharacteristic * pRxCharacteristic = pService->createCharacteristic(
											 CHARACTERISTIC_UUID_RX,
											BLECharacteristic::PROPERTY_WRITE
										);

  pRxCharacteristic->setCallbacks(new MyCallbacks());

  // Start the service
  pService->start();

  // Start advertising
  pServer->getAdvertising()->start();
  Serial.println("Waiting a client connection to notify...");
}
uint8_t dummy=0;

void loop() {

	userScheduler.execute();
    if(deviceConnected && result_send){
	  pTxCharacteristic->setValue(resultparser(step_l).c_str());
      pTxCharacteristic->notify();
	  if(step_l!=4) step_l++;
	  else {
	    i++;
		if(i>=115) step_l++;
	  }
	  if(step_l>5) result_send=false;
	  delay(25);
    }
    

    // disconnecting
    if (!deviceConnected && oldDeviceConnected) {
        delay(500); // give the bluetooth stack the chance to get things ready
        pServer->startAdvertising(); // restart advertising
        Serial.println("start advertising");
        oldDeviceConnected = deviceConnected;
    }
    // connecting
    if (deviceConnected && !oldDeviceConnected) {
		// do stuff here on connecting
        oldDeviceConnected = deviceConnected;
    }
}

String resultparser(uint8_t step){
  switch(step){
    case 0: ;break;
	case 1: {
	  result="{\"value\": ";
      value.clear();
      value["FVC"] = 2.64;
      value["FEV1"] = 1.72;
      value["PEF"] = 2.46;
      value["ratio"] = 0.65;
      value["TimeSeriesLength"] = 59;
      value["XYLength"] = 115;
      serializeJson(value, result);
	  return result;
	}break;
	case 2: {
	  result=", \"graph\": ";
	  Serial.print(", \"graph\": ");
	  copyArray(vtimeseries, timeseries.to<JsonArray>());
	  result+="{\"timeseries\": ";
	  Serial.print("{\"timeseries\": ");
	  serializeJson(timeseries, result);
	  return result;
	}break;
	case 3: {
	  result=",\"XY\": [ ";
	  return result;
	}break;
	case 4: {
	  result="";
	  copyArray(vfgraph[i], graph.to<JsonArray>());
	  serializeJson(graph, result);
	  if(i<114){
	    result+=",";
	  }
	  return result;
	}break;
	case 5: {
	  result="] } }";
	  return result;
	}break;
  }
}