# 1. Bluetooth 
ESP32 support Bluetooth classic(BT) dan Bluetooth Low Energy (BLE).
|       |Android      |BLE    |
|-------|-------------|-------|
|Android|support|support|
|IOS    | not detected|support|

## 1.1 Bluetooth Classic
Nama Program:
dummySpiroBT.ino

Bisa di-compile dan jalan di board DOIT ESP32 DEVKIT V1.
Library yang diperlukan:
- ArduinoJson.h 
- TaskScheduler.h (ada di folder src)
- BluetoothSerial.h (bawaan dari board ESP32)

Pengetesan pada HP Android dilakukan menggunakan App Serial Bluetooth Terminal oleh Kai Morich.  
Program dummy ini baru bisa untuk mode spirometry.
Device Name ketika pairing dengan HP: ESP32test

## 1.2 Bluetooth Low Energy
Nama Program:
dummySpiroBLE.ino

BLE masih banyak yang saya belum mengerti, program ini hanya editan dari example BLE_uart.ino. Bisa di-compile dan jalan di board DOIT ESP32 DEVKIT V1.
Library yang diperlukan:
- ArduinoJson.h 
- TaskScheduler.h (ada di folder src)
- BLEDevice.h (bawaan dari board ESP32)
- BLEServer.h (bawaan dari board ESP32)
- BLEUtils.h (bawaan dari board ESP32)
- BLE2902.h (bawaan dari board ESP32)

Pengetesan pada HP Android dilakukan menggunakan App Serial Bluetooth Terminal oleh Kai Morich.  
Program dummy ini baru bisa untuk mode spirometry.

# 2. Command dari HP
Text dari HP yang dikirim via Bluetooth
Perintah mulai:
```sh
{"cmd":"start","mode":"spiro"}
```
perintah berhenti:
```sh
 {"cmd":"stop","mode":"spiro"}
```

# 3. Data dari Perangkat

## 3.1 Stream Volume
Setelah user mengirim perintah mulai, perangkat akan mengirim data volume setiap 100ms.
Data dikirim tanpa format apapun, hanya string dari nilai volume.

| No |Tipe      |Satuan|Interval|
|----|----------|------|--------|
|1   |float     |L     | 100ms  |

## 3.2 Spirometry Result
Data yang dikirim jika perangkat mendeteksi adanya manuver spirometry (pada dummy ini setelah lewat 25 detik).
Jika tidak terdeteksi adanya manuver spirometry, perangkat tidak mengirim apapun.

value
| No |Nama |Tipe   |Satuan|
|----|-----|-------|------|
|1   |FVC  |float  |L     |
|2   |FEV1 |float  |L     |
|3   |PEF  |float  |L/s   |
|4   |ratio|float  |      |
|5   |TimeSeriesLength  |int  |      |
|6   |XYLength  |int  |      |

graph
| No |Nama |Tipe   |Satuan|Isi array|
|----|-----|-------|------|---------|
|1   |timeseries|float|L|[volume]|
|2   |XY|[float,float]|[L,L/s]|[volume,flow]|

Contoh data:
```sh
{
	"value": {
		"FVC": 2.64,
		"FEV1": 1.72,
		"PEF": 2.46,
		"ratio": 0.65,
		"TimeSeriesLength": 59,
		"XYLength": 115
	},
	"graph": {
		"timeseries": [0, 0.14, 0.37, 0.62, 0.85, 1.05, 1.22, 1.37, 1.5, 1.61, 1.72, 1.81, 1.9, 1.98, 2.05, 2.12, 2.17, 2.23, 2.27, 2.31, 2.35, 2.39, 2.42, 2.44, 2.47, 2.49, 2.51, 2.53, 2.55, 2.55, 2.56, 2.58, 2.58, 2.59, 2.59, 2.6, 2.6, 2.61, 2.61, 2.62, 2.63, 2.63, 2.63, 2.63, 2.64, 2.64, 2.64, 2.64, 2.64, 2.64, 2.64, 2.64, 2.64, 2.63, 2.63, 2.63, 2.63, 2.63, 2.63],
		"XY": [
			[0, -0.12],
			[0.14, 1.4],
			[0.37, 2.33],
			[0.62, 2.46],
			[0.85, 2.29],
			[1.05, 2],
			[1.22, 1.74],
			[1.37, 1.44],
			[1.5, 1.3],
			[1.61, 1.19],
			[1.72, 1.09],
			[1.81, 0.9],
			[1.9, 0.87],
			[1.98, 0.74],
			[2.05, 0.75],
			[2.12, 0.64],
			[2.17, 0.57],
			[2.23, 0.52],
			[2.27, 0.47],
			[2.31, 0.41],
			[2.35, 0.38],
			[2.39, 0.34],
			[2.42, 0.3],
			[2.44, 0.29],
			[2.47, 0.27],
			[2.49, 0.23],
			[2.51, 0.19],
			[2.53, 0.17],
			[2.55, 0.13],
			[2.55, 0.09],
			[2.56, 0.09],
			[2.58, 0.13],
			[2.58, 0.04],
			[2.59, 0.07],
			[2.59, 0.04],
			[2.6, 0.04],
			[2.6, 0.07],
			[2.61, 0.06],
			[2.61, 0.02],
			[2.62, 0.06],
			[2.63, 0.07],
			[2.63, 0.02],
			[2.63, 0.01],
			[2.63, 0.04],
			[2.64, 0.08],
			[2.64, 0.01],
			[2.64, 0.03],
			[2.64, 0.01],
			[2.64, 0],
			[2.64, -0.01],
			[2.64, -0.03],
			[2.64, -0.01],
			[2.64, -0.02],
			[2.63, -0.03],
			[2.63, -0.06],
			[2.63, -0.03],
			[2.63, 0.02],
			[2.63, 0],
			[2.63, 0],
			[2.62, -0.09],
			[2.58, -0.37],
			[2.51, -0.66],
			[2.44, -0.77],
			[2.36, -0.77],
			[2.28, -0.8],
			[2.19, -0.86],
			[2.11, -0.85],
			[2.02, -0.87],
			[1.94, -0.86],
			[1.85, -0.9],
			[1.76, -0.89],
			[1.67, -0.84],
			[1.59, -0.87],
			[1.5, -0.89],
			[1.4, -0.91],
			[1.31, -0.9],
			[1.23, -0.89],
			[1.14, -0.87],
			[1.05, -0.9],
			[0.96, -0.86],
			[0.88, -0.8],
			[0.81, -0.75],
			[0.74, -0.69],
			[0.68, -0.61],
			[0.62, -0.57],
			[0.57, -0.51],
			[0.52, -0.44],
			[0.48, -0.45],
			[0.44, -0.41],
			[0.4, -0.4],
			[0.36, -0.41],
			[0.32, -0.38],
			[0.28, -0.32],
			[0.26, -0.27],
			[0.24, -0.18],
			[0.22, -0.18],
			[0.21, -0.16],
			[0.2, -0.11],
			[0.19, -0.06],
			[0.19, 0.04],
			[0.2, 0.1],
			[0.2, -0.04],
			[0.19, -0.1],
			[0.18, -0.08],
			[0.16, -0.18],
			[0.15, -0.17],
			[0.15, 0.02],
			[0.15, 0.02],
			[0.14, -0.08],
			[0.13, -0.08],
			[0.13, -0.09],
			[0.12, -0.1],
			[0.12, 0.09],
			[0.14, 0.17],
			[0.14, 0.03]
		]
	}
}
```
